#!/usr/bin/env python3

from io import StringIO
from .. import Fountain
import unittest
import json

contents = u"""
Title:
    _**BRICK & STEEL**_
    _**FULL RETIRED**_
Credit: Written by
Author: Stu Maschwitz
Source: Story by KTM
Draft date: 1/20/2012
Contact:
    Next Level Productions
    1588 Mission Dr.
    Solvang, CA 93463

INT. trailer home - Day

This is the home of THE BOY BAND, AKA DAN and JACK.  They too are drinking beer, and counting the take from their last smash-and-grab.  Money, drugs, and ridiculous props are strewn about the table.

CUT TO:

.a River

We're underwater, watching a fat catfish swim along.

EDWARD
I didn't put any stock into such speculation or superstition.  All I knew was I'd been trying to catch that fish since I was a boy no bigger than you.
(closer)
And on the day you were born, that was the day I finally caught him.

JACK (V.O.)
I'm not sure.
Maybe you should go now.

James watches Helen and Vaughan steer Seagrave into the living-
room, where two people sit on a couch watching television with the
sound turned off: Gabrielle, a sharp-faced young woman who is
rolling a hash joint; and Seagrave's wife, Vera, a handsome,
restless woman of about thirty.

> BURN TO WHITE
"""

def debug(o):
    print((json.dumps(o, sort_keys=True, indent=2)))

class TestFountain(unittest.TestCase):

    def setUp(self):
        fragmentFile = StringIO(contents)
        fountain = Fountain()
        fountain.tokenize(fragmentFile)
        self.fountain = fountain

    def testTokenizerRecognizesElementTypes(self):
        tokens = self.fountain.tokens
        self.assertEqual(tokens[0]['type'], "titles")
        self.assertEqual(tokens[1]['type'], "slugline")
        self.assertEqual(tokens[2]['type'], "action")
        self.assertEqual(tokens[3]['type'], "transition")
        self.assertEqual(tokens[4]['type'], "slugline")
        self.assertEqual(tokens[5]['type'], "action")
        self.assertEqual(tokens[6]['type'], "dialogue")
        self.assertEqual(tokens[7]['type'], "dialogue")
        self.assertEqual(tokens[8]['type'], "action")

    def testExtractSluglineElements(self):
        tokens = self.fountain.tokens
        trailer_slug = tokens[1]
        self.assertEqual(trailer_slug['location'], 'TRAILER HOME')
        self.assertEqual(trailer_slug['time'], 'DAY')

        river_slug = tokens[4]
        self.assertTrue(river_slug['forced'])
        self.assertFalse('forced' in trailer_slug)
        self.assertEqual(river_slug['description'], 'A RIVER')

    def testExtractDialogElements(self):
        edward = self.fountain.tokens[6]
        self.assertEqual(edward['character'], 'EDWARD')
        jack = self.fountain.tokens[7]
        self.assertEqual(jack['character'], 'JACK (V.O.)')
        self.assertEqual(
          jack['text'],
          [ "I'm not sure.", "Maybe you should go now." ]
        )

if __name__ == '__main__':
    unittest.main()
